// Nathaniel Brandt & Johnathan Burns
// 2018-07-19
// 3D Printing Filament measurement apparatus

// Pins 2-13 are connected to the 7 segment display.
int tim = 3; //time increment for multiplexing

// Digital Output pins
#define pinA 11
#define pinB 9
#define pinC 4
#define pinD 5
#define pinE 6
#define pinF 10
#define pinG 8
#define pinDP 13
#define D1 3
#define D2 7
#define D3 2
#define D4 12

// Analog Input pin
#define interruptPin 4 // Select the input pin for the interrupter

// Analog level of counter state transition
#define transitionValue 990

// Other variables
float val = 0; // Variable to store the value coming from the sensor
int preval = 0;
int count = 0; // Count number of times the light is seen

// Function to select the voltage levels required for a digit 0-9
void choose_digit(char num) {
  switch(num){
    default:
    // 0 default value
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinE, HIGH);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, LOW);   
      break;
      
    case 1:
      digitalWrite(pinA, LOW);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, LOW);   
      digitalWrite(pinE, LOW);   
      digitalWrite(pinF, LOW);   
      digitalWrite(pinG, LOW);   
      break;
      
    case 2:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, LOW);   
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinE, HIGH);   
      digitalWrite(pinF, LOW);   
      digitalWrite(pinG, HIGH);
      break;
      
    case 3:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinE, LOW);   
      digitalWrite(pinF, LOW);   
      digitalWrite(pinG, HIGH); 
      break;
      
    case 4:
      digitalWrite(pinA, LOW);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, LOW);   
      digitalWrite(pinE, LOW);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH);  
      break;
      
    case 5:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, LOW);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinE, LOW);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH);     
      break;
      
    case 6:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, LOW);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinE, HIGH);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH); 
      break;
      
    case 7:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, LOW);   
      digitalWrite(pinE, LOW);   
      digitalWrite(pinF, LOW);   
      digitalWrite(pinG, LOW);   
      break;
      
    case 8:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, HIGH);   
      digitalWrite(pinE, HIGH);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH);   
      break;
      
    case 9:
      digitalWrite(pinA, HIGH);   
      digitalWrite(pinB, HIGH);   
      digitalWrite(pinC, HIGH);   
      digitalWrite(pinD, LOW);   
      digitalWrite(pinE, LOW);   
      digitalWrite(pinF, HIGH);   
      digitalWrite(pinG, HIGH); 
      break;
  }
}

// Function to determine which digit slot (1-4) is illuminated
void pick_digit(int digit) {
    digitalWrite(D1, HIGH);
    digitalWrite(D2, HIGH);
    digitalWrite(D3, HIGH);
    digitalWrite(D4, HIGH); 
    
    switch(digit) {
      case 1: digitalWrite(D1, LOW); break;
      case 2: digitalWrite(D2, LOW); break;
      case 3: digitalWrite(D3, LOW); break;
      default: digitalWrite(D4, LOW); break;
    }
}

// Function to streamline number selection
void seven_segment(int number) {
    unsigned char thousands = int (number/1000);
    unsigned char hundreds  = int ((number/100)%10);
    unsigned char tens      = int ((number/10)%10);
    unsigned char ones      = int (number%10);
    
    choose_digit(thousands);
    pick_digit(1);
    delay(tim);
    
    choose_digit(hundreds);
    pick_digit(2);
    delay(tim);

    digitalWrite(pinDP, HIGH);
    choose_digit(tens);
    pick_digit(3);
    delay(tim);
    digitalWrite(pinDP, LOW);
    
    choose_digit(ones);
    pick_digit(4);
    delay(tim);
}

void setup() {
  // Set the necessary pins as outputs 
  pinMode(pinA, OUTPUT);     
  pinMode(pinB, OUTPUT);     
  pinMode(pinC, OUTPUT);     
  pinMode(pinD, OUTPUT);     
  pinMode(pinE, OUTPUT);     
  pinMode(pinF, OUTPUT);     
  pinMode(pinG, OUTPUT);   
  pinMode(pinDP, OUTPUT);
  pinMode(D1, OUTPUT);  
  pinMode(D2, OUTPUT);  
  pinMode(D3, OUTPUT);  
  pinMode(D4, OUTPUT);
}

void loop() {
    preval = val;
    val = analogRead(interruptPin); // read the value from the sensor
    
    if ((val>=transitionValue) && (preval<transitionValue)){
     seven_segment(++count);
     delay(500);
    }
    
    delay(5);
    seven_segment(count);
    //delay(10);
}
